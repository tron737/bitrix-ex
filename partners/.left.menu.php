<?php
/**
 * Created by PhpStorm.
 * User: antonankilov
 * Date: 2019-02-20
 * Time: 15:33
 */

if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();


$aMenuLinks = Array(
    Array(
        "Условия сотрудничества",
        "rules.php",
        Array(),
        Array(),
        ""
    ),
    Array(
        "Расписание мероприятий",
        "schedule/",
        Array(),
        Array(),
        ""
    ),
);